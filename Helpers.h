#pragma once

long long sum_of_squares(const int& left_operand, const int& right_operand) noexcept
{
	return static_cast<long long>(left_operand) * left_operand + static_cast<long long>(right_operand) * right_operand;
}

struct parsing_exception : std::exception
{
	std::string message;
	int code;

	parsing_exception(std::string message, int code) : message(message), code(code), std::exception() { }
};

int try_parse_operand(char* input, const std::string& operand_name)
{
	const std::string operand_raw{ input };
	int operand_value{};
	const auto [remainder, error]
	{
		std::from_chars(operand_raw.data(), operand_raw.data() + operand_raw.size(), operand_value)
	};
	if (error == std::errc::invalid_argument)
	{
		throw parsing_exception{ operand_name + " is not an integer: " + operand_raw, 0b01 };
	}
	else if (error == std::errc::result_out_of_range)
	{
		throw parsing_exception{ operand_name + " is larger than an int: " + operand_raw, 0b10 };
	}
	else if (error != std::errc())
	{
		throw parsing_exception{ "Unknown error parsing the " + operand_name + ": " + operand_raw, 0b11 };
	}

	return operand_value;
}
