#include <charconv>
#include <string_view>
#include <iostream>
#include <tuple>

#include "Helpers.h"

int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		std::cout << "Usage:" << argv[0] << " [first integer] [second integer]" << std::endl;
		std::exit(0b00001);
	}

	try
	{
		const int left_operand = try_parse_operand(argv[1], "Left operand");
		const int right_operand = try_parse_operand(argv[2], "Right operand");

		const long long result = sum_of_squares(left_operand, right_operand);

		std::cout << left_operand << "^2 + " << right_operand << "^2 = " << result << std::endl;
	}
	catch (parsing_exception& parsing_exception)
	{
		std::cerr << parsing_exception.message << std::endl;
		std::exit(parsing_exception.code);
	}
}